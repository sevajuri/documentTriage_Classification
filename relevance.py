from utilities import *
import pickle
from collections import OrderedDict
import itertools

ut = utilities()

if not path.exists('../dataSets/cancerTypes.p'):
    ut.prepareCancerTypeDict()

if not path.exists('../dataSets/civic_allRecords.p'):    
    ut.getAllCivic()
    
if not path.exists('../dataSets/civic_GROUPPED.p'):
    #f = open('../dataSets/civic_allRecords.csv','rb')
    #reader = csv.reader(f)
    #header = reader.next()
    articlesOriginal = {}
    articles = {}
    visitedDicts = {}
    noneReturned = 0
    
    reader = pickle.load(open('../dataSets/civic_allRecords.p','rb'))

    for row in reader:
        try:
            diod='DOID:'+row[u'disease_doid'].strip()
            print "---------------------------------------------------"
            print "diod: ", diod, "PMID:\t", int(row[u'source_pubmed_id'])
            print articles.keys(),'\t',  list(itertools.chain(*[articles[item].keys() for item in articles.keys()]))
            if int(row[u'source_pubmed_id']) not in list(itertools.chain(*[articles[item].keys() for item in articles.keys()])): 
            
                if diod in ut.root_doid:
                    print "OG in rootG"
                    url = 'http://www.disease-ontology.org/api/metadata/{}'.format(diod)
                    r = requests.get(url)
                    try:
                        temp = r.json()
                        dng = temp['name']
                    except (KeyError,ValueError):
                        dng = "disease"
                else:
                    print "Looking for rootG from OG"
                    dng = ut.getAllParentsDOID(diod)
                
                try:
                    articles[dng][int(row[u'source_pubmed_id'])] = {
                                                                    u'Article_ArticleTitle':row['Article_ArticleTitle'],
                                                                    u'Abstract_AbstractText':row['Abstract_AbstractText']
                                                                    }
                except:
                    articles[dng]={}
                    articles[dng][int(row[u'source_pubmed_id'])] = {
                                                                    u'Article_ArticleTitle':row['Article_ArticleTitle'],
                                                                    u'Abstract_AbstractText':row['Abstract_AbstractText']
                                                                    }
                print "Done ", diod, "\t", dng
            else:
                print 'Already analyzed:\t',diod,'\t',int(row[u'source_pubmed_id'])
        except:
            pass
            
    pickle.dump(articles, open('../dataSets/civic_GROUPPED.p','wb'))
    
grupped = pickle.load(open('../dataSets/civic_GROUPPED.p','rb'))
ordered_d = OrderedDict(sorted(grupped.viewitems(), key=lambda (k,v):len(v), reverse=True))

print noneReturned

sum = 0
for k, v in grupped.iteritems():
    print k, len(v)
    sum += len(v)
print sum