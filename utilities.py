from Bio import Entrez
from Bio.Entrez import efetch, read
import xml.etree.ElementTree as ET
import sys
import csv
import re
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from os import path
import traceback
import pubmed_parser as pp
try:
    import cPickle as pickle
except:
    import pickle
    
import urllib2, ssl
from urllib2 import HTTPError
import requests

Entrez.email = "seva@informatik.hu-berlin.de"
context = ssl._create_unverified_context()

class utilities():
    
    def __init__(self):
        #=======================================================================
        # self.root_doid = ["DOID:193" , "DOID:0060083" , "DOID:170", "DOID:176", "DOID:11934", "DOID:1725", "DOID:0060122", 
        #              "DOID:0050615", "DOID:0060100", "DOID:5875", "DOID:3119", "DOID:3093", "DOID:3996", "DOID:5093",
        #              "DOID:2994", "DOID:0070003", "DOID:3070", "DOID:2426", "DOID:3350", "DOID:305", "DOID:154", 
        #              "DOID:1115", "DOID:1790", "DOID:9256"]
        #=======================================================================
        self.root_doid = [#"DOID:219", 
                           "DOID:3571", #liver cancer
                           "DOID:1909", #melanoma
                           "DOID:9256", #colorectal cancer
                           "DOID:11934", #head and neck cancer
                           "DOID:4"] #geenral disease, root node
        
    def getDict_KV(self, dl, kl, vl):
        #keys, values from nested dictionaries
        for k, v in dl.iteritems():
            if isinstance(v, dict):
                self.getDict_KV(v, kl, vl)
            else:
                kl.append(k)
                vl.append(v)
                
    def getDict_K(self, dl, kl, prefix=''):
        #keys, values from nested dictionaries
        for k, v in dl.iteritems():
            if isinstance(v, dict):
                self.getDict_KV(v, kl, k)
            else:
                if prefix != '':
                    kl.append(keyName)
                else:
                    
                    kl.append(k)
                
    def getDict_V(self, dl, vl):
        #values from nested dictionaries
        for k, v in dl.iteritems():        
            if isinstance(v, dict):           
                getDict_V(v, vl)
            else:
                vl.append(v)
                
    def flatten_dict(self, startDict, flatDict, prefix=""):
        for k,v in startDict.iteritems():
            if isinstance(v, dict):
                self.flatten_dict(v, flatDict, k)
            else:
                #print k, v
                if prefix != '':
                    k = prefix+'_'+k
                    
                if isinstance(v, list):
                    try:
                        v = "|".join(v)
                    except:
                        v = ""
                        
                flatDict[k]=v
        return flatDict
    
    def getTextFromPMID(self, pmid):
        dummyText = None    
        try:
            handle = efetch("pubmed", id=pmid, rettype="abstract", retmode="xml")
            temp = handle.read()
            tree = ET.fromstring(temp)
            dummyText = u"". join([item.text for item in tree.findall(".//AbstractText")])
        except Exception as e: 
            print str(e)
            sys.exit(0)
        return dummyText
    
    def getMeSHFromPMID(self, pmid):
        # call PubMed API
        handle = efetch(db='pubmed', id=str(pmid), retmode='xml')
        xml_data = read(handle)[0]
        # skip articles without MeSH terms
        if u'MeshHeadingList' in xml_data['MedlineCitation']:
            for mesh in xml_data['MedlineCitation'][u'MeshHeadingList']:
                # grab the qualifier major/minor flag, if any
                major = 'N'
                qualifiers = mesh[u'QualifierName']
                if len(qualifiers) > 0:
                    major = str(qualifiers[0].attributes.items()[0][1])
                # grab descriptor name
                descr = mesh[u'DescriptorName']
                name = descr.title()
                yield(name, major)
                
    def extendWithPubMed(self, pmid):
        #=======================================================================
        # get additional information about article from pmid
        #=======================================================================
        abstractText = u''
        MeshCodes = []
        articleTitle = u''
        try:
            #get article info
            handle = efetch("pubmed", id=pmid, rettype="abstract", retmode="xml")
            xml_data = read(handle)
        except Exception as e: 
            print str(e)
            print "Unexpected error:", sys.exc_info()[0]
            traceback.print_exc()
            sys.exit(0)

        #ARTICLE TITLE
        try:
            articleTitle = u''.join(xml_data[u'MedlineCitation']['Article'][u'ArticleTitle']).encode('utf-8')
        except: 
            pass
            
        #ARTICLE/ABSTRACT TEXT
        try:
            abstractText = u''.join(xml_data[u'MedlineCitation']['Article']['Abstract']['AbstractText']).encode('utf-8')
        except: 
            pass
            
        #MESH CODES FROM ARTICLE IF ANY
        try:
            MeshCodes = ' | '.join([mesh[u'DescriptorName'].title().decode('utf-8') for mesh in xml_data['MedlineCitation'][u'MeshHeadingList']])
        except:
            pass
            
        return (abstractText, MeshCodes, articleTitle) 
    
    def prepareData(inputFile, otuputFile, i1, i2):
        mydict = {}
        with open(inputFile, mode='r') as infile:
            reader = csv.reader(infile)
            for rows in reader:
                try: 
                    key = int(re.search(r'\d+', rows[i1]).group())
                    print key
                    if key not in mydict:
                        mydict[key] = rows[i2]
                except:
                    pass
                
        pickle.dump(mydict, open(otuputFile+'.p', 'wb'))
        
    def getRecentPubMedArticles(self, age=360, nrArticles=10000):
        fetchUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=&reldate={}&datetype=edat&retmax={}&usehistory="y"'.format(age,nrArticles)
        response = urllib2.urlopen(fetchUrl, context=ssl._create_unverified_context())
        xmlResults = ET.fromstring(response.read())
     
        WebEnv = [ node.text for node in xmlResults.findall('.//WebEnv') ][0]
        count  = int([ node.text for node in xmlResults.findall('.//Count') ][0])
        QueryKey = [ node.text for node in xmlResults.findall('.//QueryKey') ][0]
        idList = ','.join( [ node.text for node in xmlResults.findall('.//Id') ] )
        print len(idList)
        raw_input('prompt')
        
        abstracts = []       
        batch_size = 100
        
        for start in range(0,count,batch_size):
            end = min(count, start+batch_size)
            print("Going to download record %i to %i" % (start+1, end))
            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",
                                             retmode="xml",retstart=start,
                                             retmax=batch_size,
                                             webenv=WebEnv,
                                             query_key=QueryKey)
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    
            #ARTICLE TITLE
            xml_data = Entrez.read(fetch_handle)
            for item in xml_data:

                flatDict = {}            
                flatDict = ut.flatten_dict(xml_data, flatDict)
                print flatDict
     
                if u'Abstract_AbstractText' in flatDict.keys() and u'Article_ArticleTitle' in flatDict.keys() and flatDict[u'Abstract_AbstractText'] and flatDict[u'Article_ArticleTitle']:
                    abstracts.append(flatDict)
 
            fetch_handle.close()
              
            if len(abstracts) > nrArticles:
                break
        
        pickle.dump(abstracts, open( "../dataSets/pubmed_recent.p", "wb" ) )
 
    def getPubMedMeSH(self,meshCodes):
        #=======================================================================
        # meshCodes -> list of mesh codes to search for
        #=======================================================================
        WebEnv = [ node.text for node in xmlResults.findall('.//WebEnv') ][0]
        count  = int([ node.text for node in xmlResults.findall('.//Count') ][0])
        QueryKey = [ node.text for node in xmlResults.findall('.//QueryKey') ][0]
        ','.join( [ node.text for node in xmlResults.findall('.//Id') ] )
        
        abstracts = []       
        batch_size = 100

        for start in range(0,count,batch_size):
            end = min(count, start+batch_size)
            print("Going to download record %i to %i" % (start+1, end))
            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",
                                             retmode="xml",retstart=start,
                                             retmax=batch_size,
                                             webenv=WebEnv,
                                             query_key=QueryKey)
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)

            data = fetch_handle.read()
            xmlResultsAb= ET.fromstring(data)

            abstracts.extend( [ node.text.encode('utf-8').strip() for node in xmlResultsAb.findall('.//AbstractText') 
                                if node.text and not ("cancer" or "tumor") in node.text.lower()] )
            fetch_handle.close()
            
            if end > 10000:
                break
            
        out_handle = open("../dataSets/recentPubMedAbstracts.txt", "wb")
                     
        for item in abstracts:
            out_handle.write("%s\n" % item)
            
        out_handle.close()
        
    def prepareCancerTypeDict_old(self):
        f = pickle.load(open('../dataSets/EvidenzlisteMTC_AbstractText.csv','rb'))
        reader = csv.reader(f)
        headers = reader.next()
        cancerDictionary = dict((el,[]) for el in headers if headers.index(el) > 4 and headers.index(el) < 53)
        
        for row in reader:
            dummy = [row.index(item) for item in row if item  if row.index(item) > 4 and row.index(item) < 53]
            
            for item in dummy:
                cancerDictionary[headers[item]].append(row[53])
                
                
        pickle.dump(cancerDictionary, open('../dataSets/cancerTypes.p','wb'))

    def prepareCancerTypeDict(self):
        f = pickle.load(open('civic_allRecords.p','rb'))
        reader = csv.reader(f)
        headers = reader.next()
        cancerDictionary = dict((el,[]) for el in headers if headers.index(el) > 4 and headers.index(el) < 53)
        
        for row in reader:
            dummy = [row.index(item) for item in row if item  if row.index(item) > 4 and row.index(item) < 53]
            
            for item in dummy:
                cancerDictionary[headers[item]].append(row[53])
                
                
        pickle.dump(cancerDictionary, open('../dataSets/cancerTypes.p','wb'))
                       
    def getAllCivic(self):
        try:
            r = requests.get('https://civic.genome.wustl.edu/api/evidence_items')
            temp = r.json()
            total_count = temp['_meta']['total_count']
        
            url = 'https://civic.genome.wustl.edu/api/evidence_items?count={}'.format(total_count)
            r = requests.get(url)
            temp = r.json()
            records = temp['records']
        
            pmid = ''
            analyzedPMID = []
            preparedDictionaries = []
            
            for item in records:
                flatDict = {}
                flatDict = self.flatten_dict(item, flatDict)
                               
                if pmid != flatDict[u'source_pubmed_id'] and flatDict[u'source_pubmed_id'] not in analyzedPMID:
                    pmid = flatDict[u'source_pubmed_id']
                    dict_out = pp.parse_xml_web(pmid, save_xml=False)
                    #abstractText, MeshCodes, articleTitle = self.extendWithPubMed(pmid)
                    flatDict["Article_ArticleTitle"]=dict_out['title']
                    flatDict["Abstract_AbstractText"]=dict_out['abstract']
                    #flatDict["MeshCodes"]=MeshCodes
                    analyzedPMID.append(pmid)
                    #pprint(flatDict)
                    
                    if flatDict["Article_ArticleTitle"] and flatDict["Abstract_AbstractText"]:
                        preparedDictionaries.append(flatDict)
                    
                    print 'pmid:\t',pmid
                else:
                    print "Skipped:\t",pmid
                print '----------------------------------'
            
            pickle.dump(preparedDictionaries, open( "../dataSets/civic_allRecords.p", "wb" ) )
            
        except Exception as e:
            print e
            traceback.print_exc()
            
        
                
    def getAllParentsDOID(self, doid):
        #===============================================================================
        # colon - DOID:219 
        # leber - DOID:3571
        # melanoma - DOID:1909 
        # head/neck - DOID:11934
        #===============================================================================
        name = None
       
        while doid not in self.root_doid:
            #print "CG: ", doid
            url = 'http://www.disease-ontology.org/api/metadata/{}'.format(doid)
            r = requests.get(url)
            try:
                temp = r.json()
                name = temp['parents'][0][1]
                doid = temp['parents'][0][2]
            except (KeyError,ValueError):
                print 'Error getAllParentsDOID {}'.format(doid)
                return "disease"
        #print "==================================================="
        return name


class textVectorizer():
    
    from sklearn.feature_extraction.text import CountVectorizer
    
    def __init__(self):
        #=======================================================================
        # textCollection = input content (list of text - LoL)
        #=======================================================================
        
        self.__textCollection = None        
        self.__preapredText = None
        self.__labels = None
        self.__testText = None
        self.__testLables = None
        
        if not path.exists("../dataSets/pubmed_recent.p"):
            ut = utilities()
            ut.getRecentPubMedArticles()
            
        if not path.exists("../dataSets/civic_allRecords.p"):
            ut = utilities()
            ut.getAllCivic()
        
    def prepareData(self, ratio, field):
        
        #=======================================================================
        # cardiacP = pickle.load(open('../dataSets/preparedAbstracts_Cardiac.p','rb')) #2500
        # cardiacDocs_train = [value for key,value in cardiacP.items()[0:1500]]
        # cardiacDocs_test = [value for key,value in cardiacP.items()[1501:1636]]
        #=======================================================================
        border = 740
        limit = int(border * ratio)
        
        noCancer = pickle.load(open('../dataSets/pubmed_recent.p', 'rb'))
        noCancerDocs = [item[field].strip() for item in noCancer if item[field]]
        noCancerDocs_train = noCancerDocs[:limit]
        noCancerDocs_test = noCancerDocs[(limit+1):(limit+1+183)]
        
        cancerP = pickle.load(open('../dataSets/civic_allRecords.p','rb')) #500
        cancerDocs = [item[field].strip() for item in cancerP if item[field]]
        cancerDocs_train = cancerDocs[0:border]
        cancerDocs_test = cancerDocs[border+1:]
        
        self.__trainText = noCancerDocs_train+cancerDocs_train
        self.__trainLabels = np.array(['notCancer']*len(noCancerDocs_train)+['cancer']*len(cancerDocs_train))
        
        self.__testText = cancerDocs_test+noCancerDocs_test
        self.__testLables = np.array(['cancer']*len(cancerDocs_test)+['notCancer']*len(noCancerDocs_test))
        

    def prepareData_Embeddings(self, ratio, field):
        #http://nadbordrozd.github.io/blog/2016/05/20/text-classification-with-word2vec/
        pass
        
    def vectorize(self, ratio, field, type = 1):
        #=======================================================================
        # type -> 1 BOW, 2 TFIDF
        #=======================================================================
        self.prepareData(ratio, field)
        #print self.__preapredText
        #print self.__testText, len(self.__testText), len(self.__testLables)
        
        if type == 1: 
        
            vectorizer = CountVectorizer(input='content',encoding='utf-8', decode_error='ignore', strip_accents=None, lowercase=True, 
                                              preprocessor=None, tokenizer=None, stop_words=None, ngram_range=(1, 2), 
                                              analyzer='word', max_df=1.0, min_df=1, max_features=None, vocabulary=None)
        elif type == 2:
            
            vectorizer = TfidfVectorizer(input='content', decode_error='ignore', strip_accents=None, lowercase=True, preprocessor=None, 
                                                      tokenizer=None, analyzer='word', stop_words=None,  ngram_range=(1, 2),  max_df=1.0, min_df=1, max_features=None, 
                                                      vocabulary=None, binary=False, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False)
        
        transformed_nGramModel = vectorizer.fit_transform(self.__trainText+self.__testText)
        joblib.dump(vectorizer, 'models/nGramModel/isCancer{}__{}.vectorizer'.format(field, ratio))
        joblib.dump(vectorizer.vocabulary_, 'models/nGramModel/isCancer{}__{}.ngram'.format(field, ratio))
        joblib.dump(transformed_nGramModel, 'models/nGramModel/isCancer{}__{}.matrix'.format(field, ratio))  
        transformed_TrainText = vectorizer.transform(self.__trainText)
        transformed_TestText = vectorizer.transform(self.__testText)
                
        return (transformed_TrainText, self.__trainLabels, transformed_TestText, self.__testLables, vectorizer, transformed_nGramModel)
    
class mlAl():
    
    def __init__(self):
        self.estimatRandomSearchParameters = {
            "LogisticRegression": {
                                   'C':  sp_randint(1, randIntMax),
                                   'solver':['newton-cg', 'lbfgs', 'liblinear']
                                },
            "SVM_SVC":{
                             'C':  sp_randint(1, randIntMax),
                             'kernel':['linear', 'poly', 'rbf', 'sigmoid'] 
                             },
            "SVM_LinearSVC":{
                             'C':  sp_randint(1, randIntMax)
                             },
            "DecisionTreeClassifier":{
                                      "criterion" : ["gini", "entropy"],
                                      "splitter" : ["best", "random"],
                                      'max_depth':sp_randint(1, randIntMax)
                                      },
            "RandomForestClassifier":{
                                      'n_estimators':sp_randint(1, randIntMax),
                                      "criterion" : ["gini", "entropy"],
                                      'max_depth':sp_randint(1, randIntMax)
                                      },
            "KNeighbors":{
                             'n_neighbors':sp_randint(1, 40),
                             'weights':['uniform', 'distance'],
                             'algorithm' : ['auto'],
                             'leaf_size': sp_randint(1, 1000)
                             },
            "SGDClassifier":{
                             'loss':['hinge', 'log', 'modified_huber', 'squared_hinge'],
                             'alpha':np.random.random_sample(n_iter_search)
                             }
                     }
        self.__names = ["DecisionTreeClassifier", "LogisticRegression", "SVM_SVC", "SVM_LinearSVC",
                    "RandomForestClassifier", "SGDClassifier"]
            
        self.__estimators = [DecisionTreeClassifier(), LogisticRegression(), SVC(),  LinearSVC(),   
                      RandomForestClassifier(), SGDClassifier()]        
        
ut = utilities()
#ut.getRecentPubMedArticles()
ut.getAllCivic()