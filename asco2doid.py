import ast
import cPickle as pickle
from pymetamap import MetaMap
import requests
from lxml import etree
from bs4 import BeautifulSoup
from utilities import utilities
import sys

mm = MetaMap.get_instance('/vol/home-vol3/wbi/sevajuri/vol2Home/tools/public_mm/bin/metamap')
parser = etree.HTMLParser()
ut = utilities()
tempDict = {}
diodDict = {}
jlFile = open('../dataSets/asco.jl','rb').readlines()
filters = ['New Systemic Agents - ', 'Screening - ', 'Other: ']
#if not path.exists('../dataSets/asco_groupped.p'):
jlFileDict = []

for line in jlFile:
    dictLine = ast.literal_eval(line)
    tempText = ''.join([dictLine['category'].strip(), dictLine['subcategory'].strip(), dictLine['title'].strip(),dictLine['Abstract'].strip()])
    if 'melanoma' in tempText.lower():
        jlFileDict.append(
                {
                'category':dictLine['category'].strip(),
                'subcategory' : dictLine['subcategory'].strip(),
                'title' : dictLine['title'].strip(),
                'abstract' : dictLine['Abstract'].strip()
               })
        
print 'jlFileDict\t',len(jlFileDict)      

for line in jlFileDict:
    category = line['category'].strip()
    subCategory = line['subcategory'].strip()
    title = line['title'].strip()
    abstract = line['abstract'].strip()
    sentences = [title, category, subCategory]
    concepts,error = mm.extract_concepts(sentences)
    dngList = []
    
    # LIMIT CONCEPTS https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1997308/table/T1/
    conceptList = set([item[3] for item in concepts if item[3]])   
    
    print 'conceptList:\t', conceptList 
    
    for item in conceptList:
        dng = None
        #print item
        queryTerms = '+'.join(item.split())
        urlQuery = 'http://www.disease-ontology.org/search?q={}'.format(queryTerms)
        #print urlQuery
        try:
            r = requests.get(urlQuery)
            soup = BeautifulSoup(r.text, 'lxml')
            tds = soup.findAll("td", { "class" : "tbl-doid" })

            if tds:
                mydivs = [item.text for item in tds]
                if mydivs:
                    for x in mydivs:
                        dng = ut.getAllParentsDOID(x)
                        if dng is not None:
                            dngList.append(dng)
        except:
            print 'Error with {}'.format(urlQuery)
            print sys.exc_info()

    print list(set(dngList)),'\n****************************'
    #raw_input('prompt')

    for item in list(set(dngList)):
        if item not in diodDict:
            diodDict[item]=[]
    
        diodDict[item].append(line)

pickle.dump(diodDict, open('../dataSets/asco_groupped_DOID.p','wb'))

#QUERY DOID TO GET DOID FOR SPECIFIC ITEM
#queryTerms = '+'.join(category.split())
#urlQuery = 'http://www.disease-ontology.org/search?q={}'.format(queryTerms)