import csv
from utilities import utilities
ut = utilities()
#Entrez.email = 'seva@informatik.hu-berlin.de'

#===============================================================================
# GET ALL CIVIC DATA
#===============================================================================
ut.getAllCivic()

#===============================================================================
# PREPARE DAMIAN ANNOTATION BY ADDING TITLE, ABSTRACT AND MESH CODES TO IT
#===============================================================================
file = '../dataSets/EvidenzlisteMTC.csv'
a = 0
dummyList = []
analyzed = []
with open(file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    #header = next(reader, None)  # skip the headers
    header = [item for item in reader.next()]
    header.append("Article_ArticleTitle")
    header.append("Abstract_AbstractText")
    header.append("MeshCodes")
    for indices, row in enumerate(reader):
        try:
            if row[53]:
                pmid = row[53].split()[0]
                if pmid.isdigit() and pmid not in analyzed:
                    print 'pmid"\t', pmid
                    dummyRow = row[:54]
                    dummyRow[53] = pmid
                    abstractText, MeshCodes, articleTitle = ut.extendWithPubMed(pmid)
                    dummyRow.append(articleTitle)
                    dummyRow.append(abstractText)
                    dummyRow.append(MeshCodes)
                    #print len(dummyRow)
                    dummyList.append(dummyRow)
                    analyzed.append(pmid)
                    print "-------------------------------------------------------"
                else:
                    print "analyzed"
                    print "-------------------------------------------------------"                    
            else:
                a += 1
                print "skipped"
                print "-------------------------------------------------------"

        except Exception as e: 
            print "error", row[53]
            print str(e)   

file_path = "../dataSets/EvidenzlisteMTC_Prepared.csv"
with open(file_path, 'wb') as outcsv:   
    #configure writer to write standard csv file
    writer = csv.writer(outcsv, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')
    writer.writerow(header)
    for item in dummyList:
        #Write item to outcsv
        writer.writerow(item)
