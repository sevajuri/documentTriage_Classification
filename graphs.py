import plotly.plotly as py
import plotly.graph_objs as go
import csv
import numpy as np

#https://github.com/kevinwuhoo/randomcolor-py
import randomcolor
import matplotlib.colors as colors

py.sign_in('seva.jurica', 'djgly2w9y7')

ratios = ['5','10','15','20']
classifiers = ['DecisionTreeClassifier', 'LogisticRegression', 'SVM_SVC', 'SVM_LinearSVC', 'RandomForestClassifier', 
               'KNeighbors', 'SGDClassifier']
values = {}
for item in ratios:
    if item not in values:
        rand_color = randomcolor.RandomColor()
        color = rand_color.generate()
        rgb = colors.hex2color(color[0])
        rc = tuple([int(255*x) for x in rgb])
        rgbFormat = 'rgb({},{},{})'.format(rc[0],rc[1],rc[2])
        values[item] = {
            'r':[],
            't':[],
            'name':"1 vs "+str(item)+" ratio", 
            'marker':dict(
                color=rgbFormat
            )
        }

    csvfile = open('classificationResults_CancerDocuments_ratio.csv','rb')
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    header = spamreader.next()        
        
    for row in spamreader:
        if row[1]==item:
            values[item]['r'].append(row[5])
            values[item]['t'].append(row[0])

goAreas = []
for item in values:
    goAreas.append(
        go.Area(
            r=values[item]['r'],
            t=values[item]['t'],
            name=values[item]['name'],
            marker=values[item]['marker'],
        )
    )


print goAreas
data = goAreas
layout = go.Layout(
    title='Document traige for Cancer with regrads to PC vs NC distribution',
    font=dict(
        size=16
    ),
    legend=dict(
        font=dict(
            size=16
        )
    ),
    radialaxis=dict(
        ticksuffix='%'
    ),
    orientation=270
)
fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='polar-area-chart', image='png')

classifiersValues = {
    'DecisionTreeClassifier':[], 
    'LogisticRegression':[], 
    'SVM_SVC':[], 
    'SVM_LinearSVC':[], 
    'RandomForestClassifier':[], 
    'KNeighbors':[], 
    'SGDClassifier':[]
}

csvfile = open('classificationResults_CancerDocuments_ratio.csv','rb')
spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
header = spamreader.next()        

for row in spamreader:
    classifiersValues[row[0]].append(row[5])
    
print classifiersValues

from plotly.tools import FigureFactory as FF
hist_data=[]
group_labels = []
for item in classifiersValues:
    group_labels.append(item)
    hist_data.append(np.asarray(classifiersValues[item],  dtype=np.float32))
    
print hist_data
print group_labels
    
# Create distplot with custom bin_size
fig = FF.create_distplot(hist_data, group_labels, bin_size=.2)

# Plot!
py.iplot(fig, filename='Distplot with Multiple Datasets', validate=False)

ratios = [5,10,15,20]
classifiers = []
traceData = []
for item in classifiersValues:
    traceData.append(
        go.Scatter(
            x = ratios,
            y = classifiersValues[item],
            mode = 'lines',
            name = item
        )
    )
    
py.iplot(traceData, filename='line-mode')