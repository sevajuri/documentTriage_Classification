import requests
from bs4 import BeautifulSoup


r = requests.get('http://meetinglibrary.asco.org/abstractbysubcategory/')
soup = BeautifulSoup(r.text,"lxml")
mydivs = soup.findAll("div", { "class" : "view-content" })
links = BeautifulSoup(str(mydivs[0]), "lxml").findAll('a')
meetings = ['http://meetinglibrary.asco.org'+item.get('href') for item in links] #ALL MEETINGS FROM ASCO
print meetings
raw_input('prompt')

for meeting in meetings:
    #ALL ARTICLES IN MEETING
    r = requests.get(meeting)
    soup = BeautifulSoup(r.text,"lxml")    
    articleContent = soup.findAll("div", { "class" : "view-content" })
    articleLinks = ['http://meetinglibrary.asco.org'+item.get('href') for item in BeautifulSoup(str(articleContent[0]), "lxml").findAll('a') if 'http' not in item.get('href')]
    
    for articleURL in articleLinks:
        #=======================================================================
        # EXTRACT NEEDED INFO FROM #
        #=======================================================================
        # title = scrapy.Field()
        # category = scrapy.Field()
        # subcategory = scrapy.Field()
        # subcategoryNr = scrapy.Field()
        # meeting = scrapy.Field()
        # sessionTypeTitle = scrapy.Field()
        # AbstractNumber = scrapy.Field()
        # Citation = scrapy.Field()
        # Authors = scrapy.Field()
        # Abstract = scrapy.Field()         
        #=======================================================================
        #try:
        r = requests.get(articleURL)
        soup = BeautifulSoup(unicode(r.text),"lxml")
        
        
        #=======================================================================
        # articleContent = soup.findAll("article")
        # 
        # if articleContent:
        #     
        #     
        #     title = BeautifulSoup(str(articleContent[0]),'lxml').find('h3').text
        #     print title
        #     print type(BeautifulSoup(str(articleContent[0]),'lxml').find('div', {'class' : 'field field-name-field-subcategories field-type-text field-label-above'}))
        #     subC = 'field field-name-field-subcategories field-type-text field-label-above'
        #     c = 'field field-name-field-abstract-tracks field-type-taxonomy-term-reference field-label-above'
        #     meet = 'field field-name-field-meeting field-type-taxonomy-term-reference field-label-above'
        #     citation = 'field field-name-field-citation field-type-text-long field-label-above'
        #     authors = 'field field-name-field-authors field-type-text-long field-label-above'
        #     abstract = 'field field-name-body field-type-text-with-summary field-label-hidden'
        #=======================================================================