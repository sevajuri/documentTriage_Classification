# _*_ coding:utf-8 _*_
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
#===============================================================================
# parameter estimation
#===============================================================================
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import precision_recall_fscore_support
from scipy.stats import randint as sp_randint
import scipy.sparse as sp

try:
    pass
except:
    pass

from os import path
import csv
import numpy as np
from utilities import *
import gc, sys, traceback, os
from tabulate import tabulate
#===============================================================================
# CONFIG DATA
#===============================================================================
bestResults = 0
n_iter_search = 20
n_jobs = 40
randIntMax = 1000
ration = [1,5,10,15,20]
fields = [u'Article_ArticleTitle', u'Abstract_AbstractText']
tv = None
os.environ["OMP_NUM_THREADS"] = str(n_jobs)  # or to whatever you want
#===============================================================================
# CLASSIFIERS
#===============================================================================

names = ["DecisionTreeClassifier", "LogisticRegression", "SVM_SVC", 
        "RandomForestClassifier", "SGDClassifier"]
#"SVM_LinearSVC",

estimators = [DecisionTreeClassifier(), LogisticRegression(), SVC(),   
          RandomForestClassifier(), SGDClassifier()]
#LinearSVC(),

#===============================================================================
# START CLASSIFICATION ALGORITHMS PARAMETER ESTIMATION PARAMETERES
#===============================================================================
estimatRandomSearchParameters = {
    "LogisticRegression": {
                           'C':  sp_randint(1, randIntMax),
                           'solver':['newton-cg', 'lbfgs', 'liblinear'],
                           'class_weight':['balanced']
                        },
    "SVM_SVC":{
                    'C':  sp_randint(1, randIntMax),
                    'kernel':['linear', 'poly', 'rbf', 'sigmoid'],
                    'class_weight':['balanced'],
                    'probability':[True]
                     },
    "SVM_LinearSVC":{
                     'C':  sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                     },
    "DecisionTreeClassifier":{
                              "criterion" : ["gini", "entropy"],
                              "splitter" : ["best", "random"],
                              'max_depth':sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                              },
    "RandomForestClassifier":{
                              'n_estimators':sp_randint(1, randIntMax),
                              "criterion" : ["gini", "entropy"],
                              'max_depth':sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                              },
    "KNeighbors":{
                     'n_neighbors':sp_randint(1, 40),
                     'weights':['uniform', 'distance'],
                     'algorithm' : ['auto'],
                     'leaf_size': sp_randint(1, 1000),
                           'class_weight':['balanced']
                     },
    "SGDClassifier":{
                     'loss':['hinge', 'log', 'modified_huber', 'squared_hinge'],
                     'alpha':np.random.random_sample(n_iter_search),
                           'class_weight':['balanced']
                     }
             }

#===============================================================================
# STORE RESULTS
#===============================================================================
super_dict = {}
tabulateTable = []
classificationTestData = 'results/isCancer_ratio_civic_macro.csv'
if path.exists(classificationTestData):

    f = open(classificationTestData,'ab+')
    reader = csv.reader(f, delimiter =',',quotechar ='"',quoting=csv.QUOTE_ALL)
    header = reader.next()

    for d in reader:
        #print d[0], d[1], d[2], d[5]
        if d[0] not in super_dict:
            super_dict[d[0]]={}

        if d[1] not in super_dict[d[0]]:
            super_dict[d[0]][d[1]]={}

        super_dict[d[0]][d[1]][int(d[2])]=True

        if d[3] > bestResults:
            bestResults=d[3]

    f = open(classificationTestData,'ab+')
    writer = csv.writer(f, delimiter =',',quotechar ='"',quoting=csv.QUOTE_ALL)

else:
    f = open(classificationTestData,'wb')
    writer = csv.writer(f, delimiter =',',quotechar ='"',quoting=csv.QUOTE_ALL)
    writer.writerow( ['Field','ClassificationMethod','Ratio','BestScore', 'P', 'R', 'F','BestParams'] )

for field in fields:

    if field not in super_dict:
        super_dict[field]={}    

    for ratio in ration:
        #===============================================================================
        # PREAPRE DATA
        #===============================================================================
        #tv = textVectorizer()
        #X_train, Y_train, X_test, Y_test = tv.vectorize(ratio, field)
        #print X_train, type(Y_train)
        
        #===========================================================================
        # CREATE MODEL ON TRAIN DATA AND TEST WITH TEST DATA
        #===========================================================================
        for i, model in enumerate(estimators):
            if names[i] not in super_dict[field]:
                super_dict[field][names[i]]={}
        
            if ratio not in super_dict[field][names[i]]:
                
                try: 
                    
                    if tv == None:
                        tv = textVectorizer()
                        X_train, Y_train, X_test, Y_test, vectorizer, transformed_nGramModel = tv.vectorize(ratio, field)
                        data = sp.vstack((X_test, X_train), format='csr')
                        lables =  np.concatenate((Y_test, Y_train), axis=0)
                        
                    #===========================================================
                    # print type(X_test), type(Y_test), type(X_train), type(Y_train)
                    # raw_input('prompt')
                    #===========================================================
                    
                    #print 'Working with dictionary key {} classifier {} and ratio {}'.format(field, names[i], ratio)
                                    
                    if names[i] == 'KNeighbors':
                        estimatRandomSearchParameters[names[i]]['n_neighbors']=sp_randint(1,int(len(Y_train)*0.5)-1)
            
                    random_search = RandomizedSearchCV(model, param_distributions=estimatRandomSearchParameters[names[i]],
                                                       n_iter=n_iter_search, n_jobs=15, cv=10, pre_dispatch=30, scoring='f1_macro')
                    
                    #print cross_val_score(random_search, X_digits, y_digits)
                    random_search.fit(X_train, Y_train)                    
                    bestModel = random_search.best_estimator_.fit(X_train, Y_train)
                    y_predicted = bestModel.predict(X_test)
                    p,r,f,_ = precision_recall_fscore_support(Y_test, y_predicted, pos_label=None, average='macro')
                    
                    classModel = random_search.best_estimator_.fit(data, lables)
                    joblib.dump(bestModel, 'models/isCancer/isCancer_{}_{}_{}.classifier'.format(names[i],field,  ratio))                    
                    
                    #===========================================================
                    # create best model
                    #===========================================================
                    if random_search.best_score_ > bestResults:
                        bestResults = random_search.best_score_ 
                        #data = sp.vstack((X_test, X_train), format='csr')
                        #lables =  np.concatenate((Y_test, Y_train), axis=0)
                        #bestModel = random_search.best_estimator_.fit(data, lables)
                        joblib.dump(vectorizer, 'models/best/isCancer_best.vectorizer')
                        joblib.dump(vectorizer.vocabulary_, 'models/best/isCancer_best.ngram')
                        joblib.dump(transformed_nGramModel, 'models/best/isCancer_best.matrix')  
                        joblib.dump(bestModel, 'models/best/isCancer_best.classifier')
                        
                    #print classification_report(Y_test, y_predicted)
                    #print confusion_matrix(Y_test, y_predicted)        
                    print  '{}\t {}\t {}\t {}\t {}\t {}\t {}'.format(field, names[i], ratio, random_search.best_score_, f, p, r)
                    
                    #===================================================================
                    # update superdict
                    #===================================================================
                    writer.writerow([field, names[i], ratio, random_search.best_score_, p, r, f, random_search.best_params_])
                    tabulateTable.append([field, names[i], ratio, random_search.best_score_, p, r, f, random_search.best_params_])
                    #writer.writerow([field, names[i], ratio, random_search.best_score_, random_search.best_params_])
                    super_dict[field][names[i]][ratio] = True
                    
                    #print "####################################################################\n"        
                except Exception as e: 
                    print 'error:\t',e
                    traceback.print_stack()
                    traceback.print_exc()
                    sys.exit()
            else:
                print "Dictionary key {} on {} with ratio {} already analyzed".format(field, names[i], ratio)
                
        del tv
        tv = None
        X_train = None
        Y_train = None 
        X_test = None 
        Y_test = None
        gc.collect()
        
print tabulate(tabulateTable, headers=['Field','ClassificationMethod','Ratio','BestScore', 'P', 'R', 'F','BestParams'])