#===============================================================================
# classify clinically relevant documents for the VIST platform
#===============================================================================
import csv
import pubmed_parser as pp
import pandas as pd
import progressbar
import cPickle as pickle
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
from tqdm import tqdm
from collections import Counter
import os
from api.util import *
import traceback

def prepareCSV(file, index):
    positiveF = open(file ,'rb')
    positiveCSV=csv.reader(positiveF, delimiter='\t')
    #positiveCSV.next()
    positivePMIDS=[item[index].split(',') for item in positiveCSV]    
    return positivePMIDS

def getAbstract(inputData):
    pmid, label = inputData
    abstract = None
    pmids = []
    dict_out = pp.parse_xml_web(pmid, save_xml=False)
    if dict_out['title'] and dict_out['abstract']:
        abstract = [ label,  dict_out['title'].strip()+' '+ dict_out['abstract'].strip()  ]
    return abstract

def getAbstracts(pmidList, label):
    abstracts = []
    pmids = []
    p = Pool(processes=70)
    pmidList = [(x, label) for x in pmidList]
    
    try: 
        for res in tqdm(p.imap_unordered(getAbstract, pmidList), total=len(pmidList)):    
            if res:
                abstracts.append(res)
            
    except KeyboardInterrupt:
        sys.exit(1)
        
    except Exception as e:
        print e          
        print traceback.print_exc()

    return abstracts

#===============================================================================
# PREAPRE TRAINING DATA
# based on OncoKB
#===============================================================================
print "loading train data"
try:
    #trainDataSet = pickle.load( open( "datasets/trainDataSet.p", "rb" ) )
    trainDataSet = pickle.load( open( "datasets/trainDataSet_balanced.p", "rb" ) )
    trainDataSet_OncoKB=pickle.load( open( "datasets/trainDataSet_OncoKB.p", "rb" ) )
    trainDataSet_combined = pickle.load( open( "datasets/trainDataSet_OncoKB_CIViC.p", "rb" ) )
except IOError:
    dataSet = []

    #Download and prepare all files        
    negativePMIDS=list(set([item.strip() for sublist in prepareCSV('datasets/allAnnotatedVariants.txt', -2) for item in sublist]))
    positivePMIDS = list(set([item.strip() for sublist in prepareCSV('datasets/allActionableVariants.txt', -2) for item in sublist]))
    positiveCivicPMIDS = list(set([item.strip() for sublist in prepareCSV('datasets/nightly-ClinicalEvidenceSummaries.tsv', 11) for item in sublist]))

    #OncoKB overlap
    overlap = set(positivePMIDS).intersection(negativePMIDS)
    positivePMIDS_onco=[item for item in positivePMIDS if item not in overlap]
    negativePMIDS_onco=[item for item in negativePMIDS if item not in overlap]

    positiveText = getAbstracts(positivePMIDS_onco, "Relevant")
    negativeText = getAbstracts(negativePMIDS_onco, "NotRelevant")
    print "Original number of articles:\tPositive{}\tNegative{}".format(len(positiveText), len(negativeText))
    
    #OncoKB 
    trainDataSet_OncoKB = positiveText+negativeText
    pickle.dump( trainDataSet_OncoKB , open( "datasets/trainDataSet_OncoKB.p", "wb" ) )

    #===========================================================================
    # #OncoKB
    # multiplyBy = len(negativeText)/len(positiveText)
    # trainDataSet = positiveText*10+negativeText
    # pickle.dump( trainDataSet , open( "datasets/trainDataSet_OncoKB_multiplied.p", "wb" ) )    
    #===========================================================================

    #OncoKB balanced
    positiveText_balanced = positiveText[:min(len(positiveText), len(negativeText))]
    negativeText_balanced = negativeText[:min(len(positiveText), len(negativeText))]
    print "Balanced nubmer of articles:\tPositive{}\tNegative{}".format(len(positiveText_balanced), len(negativeText_balanced))
    trainDataSet = positiveText_balanced+negativeText_balanced        
    pickle.dump( trainDataSet , open( "datasets/trainDataSet_balanced.p", "wb" ) )
        
    #OncoKB + CIViC
    #positivePMIDS = 
    positivePMIDS.extend(positiveCivicPMIDS)
    #print positivePMIDS
    positivePMIDS = list(set(positivePMIDS))
    overlap = set(positivePMIDS).intersection(negativePMIDS)
    positivePMIDS=[item for item in positivePMIDS if item not in overlap]
    negativePMIDS=[item for item in negativePMIDS if item not in overlap]
    positiveText = getAbstracts(positivePMIDS, "Relevant")
    negativeText = getAbstracts(negativePMIDS, "NotRelevant")    
    print "OncoKB+CIViC number of articles:\tPositive{}\tNegative{}".format(len(positiveText), len(negativeText))
    trainDataSet_combined = positiveText+negativeText
    pickle.dump( trainDataSet_combined , open( "datasets/trainDataSet_OncoKB_CIViC.p", "wb" ) )

#===============================================================================
# PREPARE TESTING DATA
# based on evaluation of VIST (relevant+highly relevant vs matches the query but not clinically relevant
#===============================================================================
print "Loading test data"
try:
    testDataSet = pickle.load( open( "datasets/testDataSet.p", "rb" ) )
except IOError:

    source = 'datasets/vistEvaluations_finished.csv'
    #negativeLabel = ["Matches but not relevant"]
    positiveLabels = ["Highly Relevant", "Relevant"]

    df = pd.read_csv(source)
    users = df['User ID'].unique().tolist()
    
    userPMID = {}    
    for x in users:
        userPMID[x]=df.loc[(df['User ID'] == x) & (df['Relevant for query'].isin(positiveLabels)), 'PMID' ].tolist()
    positivePMID=list(reduce(set.intersection, [set(item) for key, item in userPMID.iteritems() ]))

    userPMID = {}
    for x in users:
        userPMID[x]=df.loc[(df['User ID'] == x) & (~df['Relevant for query'].isin(positiveLabels)), 'PMID' ].tolist()
    negativePMID=list(reduce(set.intersection, [set(item) for key, item in userPMID.iteritems() ]))    

    overlap = set(positivePMID).intersection(negativePMID)
    
    print len(positivePMID), len(negativePMID), len(overlap)

    positivePMIDS=[item for item in positivePMID if item not in overlap]
    negativePMIDS=[item for item in negativePMID if item not in overlap]

    positiveTestText = getAbstracts(positivePMIDS, "Relevant")
    negativeTestText = getAbstracts(negativePMIDS, "NotRelevant")
    testDataSet = positiveTestText+negativeTestText
    pickle.dump( testDataSet , open( "datasets/testDataSet.p", "wb" ) )

#===============================================================================
# TRAIN AND EVALUATE MODELS
#===============================================================================
testDS = pickle.load(open('datasets/testDataSet.p','rb'))
testlabels = [x[0].strip() for x in testDS]
testData = [x[1].strip() for x in testDS]

trainDatasets = {
        'clinicalRelevance_OncoKB_balanced':trainDataSet,
        'clinicalRelevance_OncoKB_original':trainDataSet_OncoKB,
        'clinicalRelevance_OncoKB_CIViC':trainDataSet_combined
     }

for k, v in trainDatasets.iteritems():
    labels = [x[0] for x in v]
    data = [x[1] for x in v]    
    print "Training on {} with {} documents distrbiuted as {}".format(k, len(trainDataSet), Counter(labels))
    #print "Ration of train data:", Counter(labels)     
    
    print "Testing with {} documents with distribution of classes {}".format(len(testDataSet), Counter(testlabels))
    #print "Ration of test data:", Counter(testlabels)
    
    bm = getBestModel('models/clinical/','clinicalRelevance_{}'.format(k), data, labels, 80, testData, testlabels)
    bm.getBestModel(k, 'Relevant')
    