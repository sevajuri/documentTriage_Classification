#===============================================================================
# colon - DOID:219 
# leber - DOID:3571
# melanoma - DOID:1909 
# head/neck - DOID:11934
#===============================================================================

import pickle

# _*_ coding:utf-8 _*_
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

#===============================================================================
# parameter estimation
#===============================================================================
from sklearn.model_selection import RandomizedSearchCV
from sklearn.externals import joblib
from scipy.stats import randint as sp_randint

try:
    import cPickle as pickle
except:
    import pickle

from os import path
import csv
import numpy as np
from utilities import *
import traceback
#===============================================================================
# CONFIG DATA
#===============================================================================
bestResults = 0
n_iter_search = 20
n_jobs = 40
randIntMax = 1000
ration = [1,5,10,15,20]
fields = [u'Article_ArticleTitle', u'Abstract_AbstractText']
#tv = None
#ml = mlAl

grupped = pickle.load(open('../dataSets/civic_GROUPPED.p','rb'))
cancerP = pickle.load(open('../dataSets/civic_allRecords.p','rb'))

names = ["DecisionTreeClassifier", "LogisticRegression", "SVM_SVC", "RandomForestClassifier", "SGDClassifier"]
#"SVM_LinearSVC",
estimators = [DecisionTreeClassifier(), LogisticRegression(), SVC(), RandomForestClassifier(), SGDClassifier()]
#  LinearSVC(),


estimatRandomSearchParameters = {
    "LogisticRegression": {
                           'C':  sp_randint(1, randIntMax),
                           'solver':['newton-cg', 'lbfgs', 'liblinear'],
                           'class_weight':['balanced']
                        },
    "SVM_SVC":{
                     'C':  sp_randint(1, randIntMax),
                     'kernel':['linear', 'poly', 'rbf', 'sigmoid'],
                           'class_weight':['balanced'] , 
                    'probability':[True]
                     },
    "SVM_LinearSVC":{
                     'C':  sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                     },
    "DecisionTreeClassifier":{
                              "criterion" : ["gini", "entropy"],
                              "splitter" : ["best", "random"],
                              'max_depth':sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                              },
    "RandomForestClassifier":{
                              'n_estimators':sp_randint(1, randIntMax),
                              "criterion" : ["gini", "entropy"],
                              'max_depth':sp_randint(1, randIntMax),
                           'class_weight':['balanced']
                              },
    "KNeighbors":{
                     'n_neighbors':sp_randint(1, 40),
                     'weights':['uniform', 'distance'],
                     'algorithm' : ['auto'],
                     'leaf_size': sp_randint(1, 1000),
                           'class_weight':['balanced']
                     },
    "SGDClassifier":{
                     'loss':['hinge', 'log', 'modified_huber', 'squared_hinge'],
                     'alpha':np.random.random_sample(n_iter_search),
                           'class_weight':['balanced']
                     }
             }

#===============================================================================
# STORE RESULTS
#===============================================================================
super_dict = {}
classificationTestData = 'results/whichCancer_macro.csv'

if path.exists(classificationTestData):

    f = open(classificationTestData,'ab+')
    reader = csv.reader(f, delimiter =',',quotechar ='"')
    header = reader.next()

    for d in reader:
        #print d[0], d[1], d[2], d[5]                   
        super_dict[d[0]]=True   
        
    f = open(classificationTestData,'ab+')
    writer = csv.writer(f, delimiter =',',quotechar ='"',quoting=csv.QUOTE_ALL)
            
else:
    f = open(classificationTestData,'wb')
    writer = csv.writer(f, delimiter =',',quotechar ='"',quoting=csv.QUOTE_ALL)
    writer.writerow( ['ClassificationMethod','bestScore','BestParams'] )

#===============================================================================
# PREPARE DATA, SPLIT TRAIN TEST (80/20), USE TRAIN TO FOR CROSS VALIDATION AND HZPERPARAMETER OPTIMIZATION
#===============================================================================
data = []
labels = []

#===============================================================================
# print grupped.keys()
# raw_input('prompt')
#===============================================================================
for item in grupped.keys():
    if item != u'liver cancer':
        for x in grupped[item].keys():
            data.append(". ".join([grupped[item][x][z] for z in grupped[item][x].keys()] ) )
            labels.append(item)
            
vectorizer = CountVectorizer(input='content',encoding='utf-8', decode_error='ignore', strip_accents=None, lowercase=True, 
                                  preprocessor=None, tokenizer=None, stop_words=None, ngram_range=(1, 2), 
                                  analyzer='word', max_df=1.0, min_df=1, max_features=None, vocabulary=None)            

#print len(data)
data_Transformed = vectorizer.fit_transform(data).toarray()
joblib.dump(vectorizer, 'models/nGramModel/whichCancer.vectorizer')
joblib.dump(vectorizer.vocabulary_, 'models/nGramModel/whichCancer.ngram')
joblib.dump(data_Transformed, 'models/nGramModel/whichCancer.matrix')

joblib.dump(vectorizer, 'models/best/whichCancer.vectorizer')
joblib.dump(vectorizer.vocabulary_, 'models/best/whichCancer.ngram')
joblib.dump(data_Transformed, 'models/best/whichCancer.matrix')

#print data_Transformed.shape

X_train, X_test, Y_train, Y_test = train_test_split(data_Transformed, labels, test_size=0.25, random_state=42)
#print len(X_train), len(y_train)


#===========================================================================
# CREATE MODEL ON TRAIN DATA AND TEST WITH TEST DATA
#===========================================================================
for i, model in enumerate(estimators):

    if names[i] not in super_dict:

        try: 

            print 'Working with classifier {}'.format(names[i])

            if names[i] == 'KNeighbors':
                estimatRandomSearchParameters[names[i]]['n_neighbors']=sp_randint(1,int(len(Y_train)*0.5)-1)
    
            clf = model
            random_search = RandomizedSearchCV(clf, param_distributions=estimatRandomSearchParameters[names[i]],
                                               n_iter=n_iter_search, n_jobs=15, cv=10, pre_dispatch=30, scoring='f1_macro')
            
            #print cross_val_score(random_search, X_digits, y_digits)
            random_search.fit(X_train, Y_train)
            
            #start = time.time()
            bestModel = random_search.best_estimator_.fit(data_Transformed, labels)
            joblib.dump(bestModel, 'models/whichCancer/'+names[i]+'.classifier')
            #y_predicted = bestModel.predict(X_test)
            #p,r,f,_ = precision_recall_fscore_support(Y_test, y_predicted, pos_label=None, average='macro')
            #end = time.time()
            #elapsed = end - start
    
            writer.writerow([names[i], random_search.best_score_, random_search.best_params_])
            #print '{} results'.format(names[i])
            #print classification_report(Y_test, y_predicted)
            #print confusion_matrix(Y_test, y_predicted)        
            print random_search.best_score_, bestResults
            
            if random_search.best_score_ > bestResults:
                print 'better than the best! '
                bestResults = random_search.best_score_
                #bestModel = random_search.best_estimator_.fit(data_Transformed, labels) 
                joblib.dump(bestModel, 'models/best/which_best.classifier')

            #===================================================================
            # update superdict
            #===================================================================
            super_dict[names[i]] = True                       
            print "####################################################################\n"        
        except Exception, e: 
            print 'error', e
            traceback.print_stack()
    else:
        print "{} already analyzed".format(names[i])
